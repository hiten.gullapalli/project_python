"""
Unit tests for the calculator library
"""

import mathop


class TestCalculator:

    def test_addition(self):
        assert 4 == mathop.add(2, 2)

    def test_subtraction(self):
        assert 2 == mathop.subtract(4, 2)

    def test_multiplication(self):
        assert 100 == mathop.multiply(10, 10)